<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () {
    return redirect('/pokemon');
});

// GET index()
$app->get('/pokemon', 'PokemonController@index');

// POST
$app->post('/pokemon', 'PokemonController@store');

// Regex is expensive but effective.
// An example named route.
$app->get('/pokemon/{id:[\d]+}',[
	'as' => 'pokemon.show',
    'uses' => 'PokemonController@show'
]);

// PUT $id
$app->put('/pokemon/{id}', 'PokemonController@update');

// DELETE $id
$app->delete('/pokemon/{id:[\d]+}', 'PokemonController@destroy');

